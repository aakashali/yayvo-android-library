package com.example.mylibrary;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

class MyAdapter extends FragmentPagerAdapter {
    private int intItems;
    private String itemDetails;

    MyAdapter(@NonNull FragmentManager fm, int behavior, String itemDetails) {
        super(fm, behavior);
        this.intItems = behavior;
        this.itemDetails = itemDetails;

    }

    /**
     * Return fragment with respect to Position .
     */
    @NonNull
    @Override
    public Fragment getItem(int position) {
        try {
            switch (position) {
                case 0:
                    CartItem CartItem = new CartItem();
                    CartItem.setArguments(setDataToCartView(getItemData(itemDetails)));
                    return CartItem;

                case 1:

                    ProductDescription productDescription = new ProductDescription();
                    productDescription.setArguments(setDataToDescriptionView(getItemData(itemDetails)));
                    return productDescription;
                case 2:
                    return new ProductSpecification();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getCount() {
        return intItems;
    }

    /**
     * This method returns the title of the tab according to the position.
     */
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Cart Details";
            case 1:
                return "Product Description";
            case 2:
                return "Product Specification";
        }
        return null;
    }

    private JSONObject getItemData(String data) throws JSONException {
        JSONObject dataObj = null;
        try {
            dataObj = new JSONObject(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataObj;
    }

    private Bundle setDataToCartView(JSONObject itemDetail) throws JSONException{
        Bundle bundle = new Bundle();
        bundle.putString("name", itemDetail.getString("name"));
        bundle.putString("short_description", itemDetail.getString("short_description"));
        return bundle;
    }

    private Bundle setDataToDescriptionView(JSONObject itemDetail) throws JSONException{
        Bundle bundle = new Bundle();
        bundle.putString("description", itemDetail.getString("description"));
        return bundle;
    }

    private Bundle setDataToSpecificationView(JSONObject itemDetail) throws JSONException{
        Bundle bundle = new Bundle();
        bundle.putString("specification", itemDetail.getString("description"));
        return bundle;
    }


}
