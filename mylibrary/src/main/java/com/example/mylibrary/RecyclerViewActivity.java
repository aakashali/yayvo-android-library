package com.example.mylibrary;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
//import androidx.recyclerview.widget.RecyclerView;

//import com.example.mylibrary

//import com.google.android.material.navigation.NavigationView;
//import com.mycompany.apiyayvo.ApiYayvo;

//import org.json.JSONArray;
import com.example.mylibrary.utils.RenderFragments;
import com.google.android.material.navigation.NavigationView;
import com.mycompany.apiyayvo.ApiYayvo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import fragments.ProductListing;

import static com.example.mylibrary.R.layout.recycler_view;

//import static com.example.myapplication.R.layout.payment_method;

public class RecyclerViewActivity extends AppCompatActivity {
    RecyclerView view;
    JSONArray productArray;
    private RecyclerViewMenu RecyclerViewMenu;
    private LinearLayoutManager RecyclerViewLayoutManager;
    private LinearLayoutManager HorizontalLayout;
    private ArrayList Number;
    private DrawerLayout mDrawerLayout;
    private RecylerAdapter recylerAdapter;
    Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.recycler_view);
        setContentView(recycler_view);

        try {
            Intent intent = getIntent();
            String userName = intent.getStringExtra("userName");
            String password = intent.getStringExtra("password");

            ApiYayvo apiYayvo = new ApiYayvo();
            apiYayvo.LoginUser(userName, password);

            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_navigation);

            /* START navigation Drawer */
            NavigationView navigationView = (NavigationView) findViewById(R.id.my_nav);
            NavigationDrawer navigationDrawer = new NavigationDrawer(navigationView,mDrawerLayout, this);
            navigationView.setNavigationItemSelectedListener(navigationDrawer);
            navigationDrawer.showNavigationDrawer(apiYayvo);
            /* END navigation Drawer **/

            // Show menu on view
            showMenu(apiYayvo);

            JSONObject productList = new JSONObject(apiYayvo.getProducts(10,1,"product_position","asc"));
            String data = productList.getString("data");


            Fragment fragment = new ProductListing();
            Bundle bundle = new Bundle();
            bundle.putSerializable("data",  data);
            fragment.setArguments(bundle);
            RenderFragments.displayFragment(this, fragment, mDrawerLayout);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void showMenu(ApiYayvo data){
        try {

            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview1);

            RecyclerViewLayoutManager = new LinearLayoutManager(getApplicationContext());

            recyclerView.setLayoutManager(RecyclerViewLayoutManager);
            JSONArray menu = new JSONArray(data.getMenu());

            /* Call data to adapter to Render data to View */
            RecyclerViewMenu = new RecyclerViewMenu(menu);

            HorizontalLayout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(HorizontalLayout);

            recyclerView.setAdapter(RecyclerViewMenu);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
