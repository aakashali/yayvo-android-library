package com.example.mylibrary;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import me.relex.circleindicator.CircleIndicator;

public class CartItem extends Fragment {
    private TextView productName, shortDescription;
    String productDetail;

    public CartItem() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.product_detail_page_main, container, false);
        //call Field by id
        productName = (TextView) view.findViewById(R.id.pdp_name);
        shortDescription = (TextView) view.findViewById(R.id.pdp_short_description);
        // set data into fields
        productName.setText(getProductInfo("name"));
        shortDescription.setText(getProductInfo("short_description"));

        String[] image = new String[]{
                "https://picsum.photos/id/458/300/200",
                "https://picsum.photos/id/459/300/200",
                "https://picsum.photos/id/457/300/200",
                "https://picsum.photos/id/456/300/200",
                "https://picsum.photos/id/455/300/200",

        };

        CircleIndicator indicator = view.findViewById(R.id.indicator);

        ViewPager viewPager = view.findViewById(R.id.image_viewer);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(view.getContext(), image);
        viewPager.setAdapter(viewPagerAdapter);
        indicator.setViewPager(viewPager);

//        return inflater.inflate(R.layout.product_detail_page_main, container, false);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

            if (getArguments() != null) {
//                productName = getArguments().getString("name")
////                String data = getArguments().getString("itemDetail");
//                try {
//                    JSONObject jsonObj = new JSONObject(getArguments().getString("itemDetail"));
//                    System.out.println(jsonObj.getString("name"));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                productDetail = getArguments().getString("itemDetail");
            }
    }

    /*This method will get information from products object*/
    private String getProductInfo(String data) {
        String productInfo = null;
        if (getArguments() != null) {
            productInfo = getArguments().getString(data);
        }
        return productInfo;
    }

}

