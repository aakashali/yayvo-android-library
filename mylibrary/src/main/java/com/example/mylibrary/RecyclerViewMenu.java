package com.example.mylibrary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

class RecyclerViewMenu extends RecyclerView.Adapter<RecyclerViewMenu.MyView> {

    private JSONArray list;

    class MyView extends RecyclerView.ViewHolder {

        TextView textView;
        RelativeLayout relativeLayout;
        ImageView menuImg;
        RecyclerView mainView;
        Context context;


        MyView(View view) {
            super(view);
            this.relativeLayout = (RelativeLayout) view.findViewById(R.id.category_layout);
            this.textView = (TextView) view.findViewById(R.id.textview1);
            this.mainView = (RecyclerView) view.findViewById(R.id.list_view);
            this.menuImg = (ImageView) view.findViewById(R.id.menu_img);
            this.context = view.getContext();

        }
    }

    public RecyclerViewMenu(JSONArray horizontalList) {
        this.list = horizontalList;
    }

    @NonNull
    @Override
    public MyView onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.category_listing, parent, false);
        return new MyView(listItem);
    }

    @Override
    public void onBindViewHolder(final MyView holder, final int position) {
        try{
            JSONObject getMenu = list.getJSONObject(position);
            System.out.println(getMenu.getString("image_url"));

            holder.textView.setText(getMenu.getString("name"));

            Picasso.get().load(getMenu.getString("name")).into(holder.menuImg);
//            holder.menuImg.
//        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                list.clear();
////                updateData(list);
////                Intent intent = new Intent(holder.context, RecyclerViewActivity.class);
////                holder.context.startActivity(intent);
//            }
//        });
        } catch (Exception e){
            e.printStackTrace();
        }

    }

//    public void updateData(List<String> horizontalList) {
//        horizontalList.clear();
////        horizontalList.addAll(horizontalList);
////        notifyDataSetChanged();
//    }

    @Override
    public int getItemCount() {
        return list.length();
    }


}
