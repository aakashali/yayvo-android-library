package com.example.mylibrary;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import me.relex.circleindicator.CircleIndicator;

public class ProductDetailPage extends AppCompatActivity {
    Intent intent;
    TextView productName, description;
    JSONObject productDetail;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail_page_main);
        try {
            productName = (TextView) findViewById(R.id.pdp_name);
            description = (TextView) findViewById(R.id.pdp_short_description);

            String[] image = new String[]{
                    "https://picsum.photos/id/458/300/200",
                    "https://picsum.photos/id/459/300/200",
                    "https://picsum.photos/id/457/300/200",
                    "https://picsum.photos/id/456/300/200",
                    "https://picsum.photos/id/455/300/200",

            };

//            System.out.println(image.length);
            intent = getIntent();

            System.out.println();
//            productDetail
            productDetail = new JSONObject(Objects.requireNonNull(intent.getStringExtra("productDetail")));
            productName.setText(getProductName(productDetail));
//            description.setText(Html.fromHtml(getString(R.string.sample_html)));
//            description.setText(getProductDescription(productDetail));
            descriptionToHtml(productDetail);
            CircleIndicator indicator = findViewById(R.id.indicator);

            ViewPager viewPager = findViewById(R.id.image_viewer);
            ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this, image);
            viewPager.setAdapter(viewPagerAdapter);
            indicator.setViewPager(viewPager);
//            String formattedText = getString(productDetail.getString("description"));

//            descriptionToHtml(formattedText);


        } catch (Exception e) {
            e.printStackTrace();
        }
//        System.out.println(productDetails);
    }

    private String getProductDetail(Intent intent){
        return  intent.getStringExtra("productDetail");
    }
    private String getProductName(JSONObject data) throws JSONException {
        return data.getString("name");
    }

    private String getProductDescription(JSONObject data) throws JSONException {
        return data.getString("description");
    }


    public void descriptionToHtml(JSONObject showDecription) throws JSONException {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            description.setText(Html.fromHtml(showDecription.getString("description"), 0));
        }

    }
}
