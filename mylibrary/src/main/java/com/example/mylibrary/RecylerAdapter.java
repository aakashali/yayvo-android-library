package com.example.mylibrary;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;


import com.squareup.picasso.Picasso;
import android.content.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecylerAdapter extends RecyclerView.Adapter<RecylerAdapter.ViewHolder> {

    private JSONArray data;
    private int dataLength;
    private Context context;


    public RecylerAdapter(JSONArray data ) {
        this.data = data;
        this.dataLength = data.length();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.cart_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            // get data
            JSONObject getData = data.getJSONObject(position);

            // Set data on textView
            holder.textView.setText(getData.getString("name"));
            holder.price.setText(getData.getString("price"));
//            holder.textDec.setText(getData.getString("description"));
            JSONObject images = new JSONObject(getData.getString("baseImage"));

//            System.out.println(images.getString("image_thumb_url"));
//            holder.textDec.setText(Html.fromHtml(getData.getString("description")));
//            holder.price.setText(getData.getString("price"));
//            String imageUrl = "https://via.placeholder.com/500";

            //Loading image using Picasso
            Picasso.get().load(images.getString("image_thumb_url")).into(holder.imageView);

            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Context context = v.getContext();
//                        Intent intent = new Intent(context, ProductDetailPage.class);
                        Intent intent = new Intent(context, ProductDetailPageCart.class);
                        intent.putExtra("productDetail", String.valueOf(data.getJSONObject(position)));

                        context.startActivity(intent);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });

            /*On Add to cart functionality */
//            holder.btn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(v.getContext(),"done",Toast.LENGTH_LONG).show();
//                }
//            });

            /* On increment button click*/
//            holder.increment.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                     int qty = Integer.parseInt(holder.editQty.getText().toString());
//                     int increment = qty+1;
//                     holder.editQty.setText(String.valueOf(increment));
//                }
//            });

            /* On decrement Button click */

//            holder.decrement.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int qty = Integer.parseInt(holder.editQty.getText().toString());
//                    int increment = qty-1;
//                    holder.editQty.setText(String.valueOf(increment));
//                }
//            });


        } catch (JSONException e) {
            e.printStackTrace();
        }

//        holder.textView.setText(data[position][0]);
//        holder.textView.setText("done");
//        holder.textDec.setText(data[position][3]);
//        holder.imageView.setImageResource(listdata[position].getImgId());
//        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(v.getContext(),data[position][1],Toast.LENGTH_LONG).show();
//            }
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(),"click on item: "+myListData.getDescription(),Toast.LENGTH_LONG).show();
//            }
//        });
    }


    @Override
    public int getItemCount() {
         return  data.length();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        RelativeLayout relativeLayout;
        TextView textDec;
        Button btn;
        Button increment;
        Button decrement;
        EditText editQty;
        TextView price;

        public LinearLayout linearLayout;

        //        public RelativeLayout relativeLayout;
        ViewHolder(View itemView) {
            super(itemView);
//            this.textView = (TextView) itemView.findViewById(R.id.re_dec);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            this.relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout);

//            this.textDec = (TextView) itemView.findViewById(R.id.description);
//            this.increment = (Button) itemView.findViewById(R.id.inc_btn);
//            this.decrement = (Button) itemView.findViewById(R.id.dec_btn);
//            this.editQty = (EditText) itemView.findViewById(R.id.edit_qty);
            this.price = (TextView) itemView.findViewById(R.id.price);
//            this.btn = (Button) itemView.findViewById(R.id.btn);
        }
    }

}
