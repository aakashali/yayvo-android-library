package com.example.mylibrary;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.example.mylibrary.utils.RenderFragments;
import com.google.android.material.navigation.NavigationView;
import com.mycompany.apiyayvo.ApiYayvo;

import org.json.JSONArray;
import org.json.JSONObject;

import fragments.ProductListing;

import static java.lang.Integer.parseInt;

class NavigationDrawer  implements NavigationView.OnNavigationItemSelectedListener{

    private NavigationView navigationView;
    private DrawerLayout mDrawerLayout;
    Context context;

    public NavigationDrawer(NavigationView navigationView, DrawerLayout mDrawerLayout, Context context) {
        this.navigationView = navigationView;
        this.mDrawerLayout = mDrawerLayout;
        this.context = context;
    }

  public void showNavigationDrawer(ApiYayvo data){
        try {
            JSONArray categories = new JSONArray(new
                    JSONObject(data.getCategoryByCity(46,2)).getString("response"));
            Menu menu = navigationView.getMenu();

            System.out.println(categories);
            for (int i = 0; i < categories.length(); i++) {

                String name = categories.getJSONObject(i).getString("name");
                String id =categories.getJSONObject(i).getString("id");

                menu.add(0, parseInt(id) , parseInt(id), name);
            }

        } catch (Exception e){
            e.printStackTrace();
        }

  }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        try {

            ApiYayvo apiYayvo = new ApiYayvo();
            JSONObject productList = new JSONObject(apiYayvo.getProducts( menuItem.getItemId(), 1, "product_position", "asc"));
            String data = productList.getString("data");

            mDrawerLayout.closeDrawer(GravityCompat.START);
            Fragment fragment = new ProductListing();
            Bundle bundle = new Bundle();
            bundle.putSerializable("data",data);
            fragment.setArguments(bundle);
            RenderFragments.displayFragment(context, fragment, mDrawerLayout);
            // Handle navigation view item clicks here.
//        switch (menuItem.getItemId()) {
//
////            case R.id.nav_maths: {
////                //do somthing
////                break;
////            }
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //close navigation drawer
//        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
