package com.example.mylibrary;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import me.relex.circleindicator.CircleIndicator;

public class ProductDetailPageCart extends AppCompatActivity {
    Intent intent;
    TextView productName, description;
    JSONObject productDetail;
    private MyAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static int int_items = 3 ;
    Dialog myCart;


    //    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.products_tab);
        intent = getIntent();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager(),int_items, getProductDetail(intent)));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });
//        setContentView(R.layout.products_tab);


//        tabLayout=(TabLayout)findViewById(R.id.tabLayout);
//
//
//        tabLayout.addTab(tabLayout.newTab().setText("Home"));
//        tabLayout.addTab(tabLayout.newTab().setText("Sport"));
//        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


            //        adapter = new MyAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
//        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
//
//        viewPager.setAdapter(adapter);




//        try {
//            productName = (TextView) findViewById(R.id.pdp_name);
//            description = (TextView) findViewById(R.id.pdp_description);
//
//            String[] image = new String[]{
//                    "https://picsum.photos/id/458/300/200",
//                    "https://picsum.photos/id/459/300/200",
//                    "https://picsum.photos/id/457/300/200",
//                    "https://picsum.photos/id/456/300/200",
//                    "https://picsum.photos/id/455/300/200",
//
//            };
//            intent = getIntent();
//
//            productDetail = new JSONObject(Objects.requireNonNull(intent.getStringExtra("productDetail")));
//            productName.setText(getProductName(productDetail));
////            description.setText(Html.fromHtml(getString(R.string.sample_html)));
////            description.setText(getProductDescription(productDetail));
//            descriptionToHtml(productDetail);
//            CircleIndicator indicator = findViewById(R.id.indicator);
//
//            ViewPager viewPager = findViewById(R.id.image_viewer);
//            ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this, image);
//            viewPager.setAdapter(viewPagerAdapter);
//            indicator.setViewPager(viewPager);
////            String formattedText = getString(productDetail.getString("description"));
////
////            descriptionToHtml(formattedText);
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
////        System.out.println(productDetails);
    }

    private String getProductDetail(Intent intent){
        return  intent.getStringExtra("productDetail");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.cart) {
            showCartItems();
            return (true);
        }

        return super.onOptionsItemSelected(item);
    }

    public void showCartItems(){
        Button btnClose;

        myCart = new Dialog(this);
        myCart.setContentView(R.layout.cart_items_popup);

        btnClose = (Button) myCart.findViewById(R.id.btn_close_popup);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCart.dismiss();
            }
        });
        // to transparent background..
        Objects.requireNonNull(myCart.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myCart.show();
    }

//    private String getProductName(JSONObject data) throws JSONException {
//        return data.getString("name");
//    }
//
//    private String getProductDescription(JSONObject data) throws JSONException {
//        return data.getString("description");
//    }
//
//
//    public void descriptionToHtml(JSONObject showDecription) throws JSONException {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            description.setText(Html.fromHtml(showDecription.getString("description"), 0));
//        }
//
//    }
}
